<?php
    require "../uteis.php";

    $usuario = new Usuario();
    if($_POST['u']['senha'] == $_POST['cSenha']){
        foreach($_POST['u'] as $key=>$value){
            $dados[$key] = ($key == 'senha'? md5($value) : $value);
        }
        $usuario->editUsuario($dados);
        $result = array(
            "status"=>'success',
            "msg"=>"Seu registro foi editado."
        );
    }else{
        $result = array(
            "status"=>'danger',
            "msg"=>"Houve um problema e o registro não foi editado."
        );
    }
    echo json_encode($result);

?>