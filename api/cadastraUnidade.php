<?php
    require "../uteis.php";

    $unidade = new Unidade();
    if($unidade->setUnidade($_POST)){
        $result = array(
            "status"=>'success',
            "msg"=>"Seu registro foi inserido com sucesso."
        );
    }else{
        $result = array(
            "status"=>'danger',
            "msg"=>"Houve um problema e o registro não foi cadastrado."
        );
    }
    echo json_encode($result);
?>