<?php
    require "../uteis.php";

    $usuario = new Usuario();
    $result = $usuario->deletaUsuario($_POST['id']);
    if($result){
        $totalRegistros = $usuario->getUsuario()['totalResults'];
        
        $result = array(
            "status"=>'success',
            "totalRegistros" => ($totalRegistros <10 ? '0'.$totalRegistros : $totalRegistros),
            "msg"=>"Seu registro foi deletado."
        );
    }else{
        $result = array(
            "status"=>'danger',
            "msg"=>"Houve um problema e o registro não foi deletado."
        );
    }
    echo json_encode($result);
?>