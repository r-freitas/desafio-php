<?php
    require "../uteis.php";

    $condominio = new Condominio();
    $result = $condominio->deletaCondominio($_POST['id']);
    if($result){
        $totalRegistros = $condominio->getCondominio()['totalResults'];
        
        $result = array(
            "status"=>'success',
            "totalRegistros" => ($totalRegistros <10 ? '0'.$totalRegistros : $totalRegistros),
            "msg"=>"Seu registro foi deletado."
        );
    }else{
        $result = array(
            "status"=>'danger',
            "msg"=>"Houve um problema e o registro não foi deletado."
        );
    }
    echo json_encode($result);
?>