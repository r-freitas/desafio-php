<?php
    require "../uteis.php";

    $bloco = new Bloco();
    if($bloco->editBloco($_POST)){
        $result = array(
            "status"=>'success',
            "totalRegistros" => ($totalRegistros <10 ? '0'.$totalRegistros : $totalRegistros),
            "msg"=>"Seu registro foi editado."
        );
    }else{
        $result = array(
            "status"=>'danger',
            "msg"=>"Houve um problema e o registro não foi editado."
        );
    }
    echo json_encode($result);
?>