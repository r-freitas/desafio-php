<?php
    require "../uteis.php";

    $morador = new Morador();
    if($morador->editMorador($_POST)){
        $result = array(
            "status"=>'success',
            "totalRegistros" => ($totalRegistros <10 ? '0'.$totalRegistros : $totalRegistros),
            "msg"=>"Seu registro foi editado."
        );
    }else{
        $result = array(
            "status"=>'danger',
            "msg"=>"Houve um problema e o registro não foi editado."
        );
    }
    echo json_encode($result);
?>