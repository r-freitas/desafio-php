<?php
    require "../uteis.php";

    $blocos = new Bloco();
    $dados = $blocos->getBlocoFromCond($_REQUEST['id']);

    if(!empty($dados)){
        $result = array(
            "status" => 'success',
            "resultSet" => $dados['resultSet']
        );

    }else{
        $result = array(
            "status" => 'success',
            "msg" => 'O cadastro não pode ser inserido.'
        );
    }

    echo json_encode($result);

?>