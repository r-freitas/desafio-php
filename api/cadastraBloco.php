<?php
    require "../uteis.php";

    $bloco = new Bloco();
    if($bloco->setBloco($_POST)){
        $result = array(
            "status"=>'success',
            "msg"=>"Seu registro foi inserido com sucesso."
        );
    }else{
        $result = array(
            "status"=>'danger',
            "msg"=>"Houve um problema e o registro não foi cadastrado."
        );
    }
    echo json_encode($result);
?>