<?php
    require "../uteis.php";

    $administradora = new Administradora();
    $result = $administradora->deletaAdministradora($_POST['id']);
    if($result){
        $totalRegistros = $administradora->getAdministradora()['totalResults'];
        
        $result = array(
            "status"=>'success',
            "totalRegistros" => ($totalRegistros <10 ? '0'.$totalRegistros : $totalRegistros),
            "msg"=>"Seu registro foi deletado."
        );
    }else{
        $result = array(
            "status"=>'danger',
            "msg"=>"Houve um problema e o registro não foi deletado."
        );
    }
    echo json_encode($result);
?>