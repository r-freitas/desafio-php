-- --------------------------------------------------------
-- Servidor:                     127.0.0.1
-- Versão do servidor:           10.4.11-MariaDB - mariadb.org binary distribution
-- OS do Servidor:               Win64
-- HeidiSQL Versão:              11.3.0.6295
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


-- Copiando estrutura do banco de dados para ap_controle
CREATE DATABASE IF NOT EXISTS `ap_controle` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `ap_controle`;

-- Copiando estrutura para tabela ap_controle.ap_administradora
CREATE TABLE IF NOT EXISTS `ap_administradora` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomeAdministradora` varchar(255) NOT NULL DEFAULT '',
  `cnpj` varchar(14) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `dataModificacao` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_administradora: ~6 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_administradora` DISABLE KEYS */;
INSERT INTO `ap_administradora` (`id`, `nomeAdministradora`, `cnpj`, `dataCadastro`, `dataModificacao`) VALUES
	(1, 'Avacon', '3126546544564', '2022-04-01 14:59:21', '2022-04-01 14:59:21'),
	(2, 'Taperinha', '332464968546', '2022-04-01 14:59:21', '2022-04-01 14:59:21'),
	(3, 'Greenbelt', '134654316546', '2022-04-01 14:59:21', '2022-04-01 14:59:21'),
	(4, 'Blackbelt', '246434546', '2022-04-05 16:53:10', '2022-04-05 16:53:11'),
	(5, 'Tsunoda', '321654967898', '2022-04-05 16:53:36', '2022-04-05 16:53:36'),
	(6, 'Biz Negócios Imobiliários', '5649649684984', '2022-04-05 16:54:07', '2022-04-05 16:54:08');
/*!40000 ALTER TABLE `ap_administradora` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_bloco
CREATE TABLE IF NOT EXISTS `ap_bloco` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_condominio` int(11) NOT NULL DEFAULT 0,
  `nomeBloco` varchar(255) NOT NULL DEFAULT '',
  `numeroAndares` int(3) NOT NULL DEFAULT 0,
  `unidadesPAndar` int(3) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `dataModificado` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chCondominioBloco` (`id_condominio`),
  CONSTRAINT `chCondominioBloco` FOREIGN KEY (`id_condominio`) REFERENCES `ap_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_bloco: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_bloco` DISABLE KEYS */;
INSERT INTO `ap_bloco` (`id`, `id_condominio`, `nomeBloco`, `numeroAndares`, `unidadesPAndar`, `dataCadastro`, `dataModificado`) VALUES
	(1, 2, 'A', 7, 6, '2022-03-29 14:23:08', '2022-03-29 14:23:10'),
	(2, 2, '1', 15, 6, '2022-03-29 14:26:09', '2022-03-29 14:26:09'),
	(4, 2, '3', 15, 6, '2022-03-29 14:26:52', '2022-03-29 14:26:52'),
	(6, 2, '5', 15, 6, '2022-03-29 14:27:27', '2022-03-29 14:27:27'),
	(7, 3, '1', 4, 16, '2022-03-29 14:31:03', NULL),
	(11, 2, 'B', 8, 6, '2022-04-01 09:30:14', NULL),
	(12, 1, 'A', 10, 4, '2022-04-01 16:25:19', NULL),
	(13, 4, 'A', 10, 6, '2022-04-01 16:47:52', NULL),
	(14, 4, '10', 4, 16, '2022-04-04 10:34:42', NULL);
/*!40000 ALTER TABLE `ap_bloco` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_condominio
CREATE TABLE IF NOT EXISTS `ap_condominio` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_administradora` int(11) NOT NULL,
  `nomeCondominio` varchar(255) NOT NULL DEFAULT '',
  `qtBlocos` int(11) NOT NULL DEFAULT 0,
  `cep` varchar(8) NOT NULL DEFAULT '',
  `logradouro` varchar(255) NOT NULL DEFAULT '',
  `numero` varchar(8) NOT NULL DEFAULT '',
  `bairro` varchar(255) NOT NULL DEFAULT '',
  `cidade` varchar(255) NOT NULL DEFAULT '',
  `estado` varchar(255) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `dataModificado` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chAdministradoraCondominio` (`id_administradora`),
  CONSTRAINT `chAdministradoraCondominio` FOREIGN KEY (`id_administradora`) REFERENCES `ap_administradora` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_condominio: ~4 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_condominio` DISABLE KEYS */;
INSERT INTO `ap_condominio` (`id`, `id_administradora`, `nomeCondominio`, `qtBlocos`, `cep`, `logradouro`, `numero`, `bairro`, `cidade`, `estado`, `dataCadastro`, `dataModificado`) VALUES
	(1, 1, 'Ed. Recanto das Montanhas', 1, '97420-20', 'Rua Floresta', '212', 'Camobi', 'Santa Maria', 'RS', '2022-03-29 14:21:14', '2022-03-29 14:21:16'),
	(2, 1, 'Condomínio Rita Vieira', 6, '94455778', 'Av. Rita Vieira', '638', 'Parque Rita Vieira', 'Campo Grande', 'MS', '2022-03-29 14:22:18', '2022-03-29 14:22:20'),
	(3, 1, 'Castelo Luxemburgo', 22, '88994649', 'Av. Senador Canale', '725', 'Universitário', 'Campo Grande', 'MS', '2022-03-29 14:30:35', NULL),
	(4, 3, 'Castelo San Marino', 20, '87000000', 'Av. Senador Canale', '816', 'Universitário', 'Campo Grande', 'MS', '2022-03-30 13:22:45', NULL);
/*!40000 ALTER TABLE `ap_condominio` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_conselho
CREATE TABLE IF NOT EXISTS `ap_conselho` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_condominio` int(11) NOT NULL DEFAULT 0,
  `funcao` enum('Sindico','Subsindico','Conselheiro') NOT NULL,
  `nomeConselho` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(255) NOT NULL DEFAULT '',
  `telefone` varchar(15) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `dataModificado` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chCondominioConselho` (`id_condominio`),
  CONSTRAINT `chCondominioConselho` FOREIGN KEY (`id_condominio`) REFERENCES `ap_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_conselho: ~12 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_conselho` DISABLE KEYS */;
INSERT INTO `ap_conselho` (`id`, `id_condominio`, `funcao`, `nomeConselho`, `cpf`, `email`, `telefone`, `dataCadastro`, `dataModificado`) VALUES
	(1, 1, 'Sindico', 'Reginaldo', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(2, 1, 'Conselheiro', 'Dirlei', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(3, 1, 'Conselheiro', 'Paulo', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(5, 1, 'Subsindico', 'Pâmela', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(8, 2, 'Subsindico', 'Leonardo', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(9, 2, 'Conselheiro', 'Maurice', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(10, 2, 'Conselheiro', 'Tiago', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(11, 2, 'Conselheiro', 'Jonas', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(12, 3, 'Sindico', 'Heitor', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(13, 3, 'Subsindico', 'Rafael', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(14, 3, 'Conselheiro', 'Gabriel', '1265438189', 'fegb@iubgfi.com', '456871386', '2022-03-30 15:03:37', '2022-03-30 15:03:39'),
	(18, 1, 'Subsindico', 'Altamir', '546565189', 'eeeh@eeehh.com', '74486484637', '2022-04-10 22:02:41', '2022-04-10 22:02:41');
/*!40000 ALTER TABLE `ap_conselho` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_morador
CREATE TABLE IF NOT EXISTS `ap_morador` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_unidade` int(11) NOT NULL DEFAULT 0,
  `id_bloco` int(11) NOT NULL DEFAULT 0,
  `id_condominio` int(11) NOT NULL DEFAULT 0,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `cpf` varchar(11) NOT NULL DEFAULT '',
  `email` varchar(11) NOT NULL DEFAULT '',
  `telefone` varchar(15) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `dataModificacao` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chCondominio` (`id_condominio`),
  KEY `chBloco` (`id_bloco`),
  KEY `chUnidade` (`id_unidade`),
  CONSTRAINT `chBloco` FOREIGN KEY (`id_bloco`) REFERENCES `ap_bloco` (`id`),
  CONSTRAINT `chCondominio` FOREIGN KEY (`id_condominio`) REFERENCES `ap_condominio` (`id`),
  CONSTRAINT `chUnidade` FOREIGN KEY (`id_unidade`) REFERENCES `ap_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=47 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_morador: ~31 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_morador` DISABLE KEYS */;
INSERT INTO `ap_morador` (`id`, `id_unidade`, `id_bloco`, `id_condominio`, `nome`, `cpf`, `email`, `telefone`, `dataCadastro`, `dataModificacao`) VALUES
	(1, 1, 1, 1, 'Reginaldo', '3216546498', 'engot@huh.c', '6799884466', '2022-04-06 13:44:45', NULL),
	(2, 2, 2, 2, 'Bruno', '5465463546', 'defi@ndiuwa', '168471684', '2022-04-06 13:44:45', NULL),
	(7, 1, 1, 1, 'Fernanda', '516514', '1654641@dhu', '314654131', '2022-04-06 13:44:45', NULL),
	(10, 1, 1, 1, 'Tiago', '2316541654', 'wsbcib@iaub', '14646168486', '2022-04-06 13:44:45', NULL),
	(11, 1, 1, 1, 'Regismar', '2316541654', 'ensjbn@ifsb', '3254686546874', '2022-04-06 13:44:45', NULL),
	(15, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(16, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(17, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(18, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(19, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(21, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(23, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(25, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(26, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(27, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(28, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(29, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(30, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(31, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(32, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(33, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(34, 17, 14, 4, 'Rita', '32165465465', 'ebf@meff.co', '45648979765', '2022-04-06 13:44:45', NULL),
	(37, 1, 1, 1, 'Regis', '16876463416', 'brocolis@ao', '3254686546874', '2022-04-06 13:44:45', NULL),
	(38, 1, 1, 1, 'Regis', '16876463416', 'brocolis@ao', '3254686546874', '2022-04-06 13:44:45', NULL),
	(39, 1, 1, 1, 'Regis', '16876463416', 'brocolis@ao', '3254686546874', '2022-04-06 13:44:45', NULL),
	(40, 1, 1, 1, 'Regis', '16876463416', 'brocolis@ao', '3254686546874', '2022-04-06 13:44:45', NULL),
	(41, 1, 1, 1, 'Regis', '16876463416', 'brocolis@ao', '3254686546874', '2022-04-06 13:44:45', NULL),
	(42, 1, 1, 1, 'Regis', '16876463416', 'brocolis@ao', '3254686546874', '2022-04-06 13:44:45', NULL),
	(43, 14, 7, 3, 'Tiago', '16876463416', 'ensjbn@ifsb', '32546865468', '2022-04-06 13:44:45', NULL);
/*!40000 ALTER TABLE `ap_morador` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_pets
CREATE TABLE IF NOT EXISTS `ap_pets` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nomePet` varchar(255) NOT NULL DEFAULT '',
  `tipo` enum('Cachorro','Gato','Passarinho') DEFAULT NULL,
  `id_morador` int(11) DEFAULT NULL,
  `dataCadastro` timestamp NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `FK__ap_morador` (`id_morador`),
  CONSTRAINT `FK__ap_morador` FOREIGN KEY (`id_morador`) REFERENCES `ap_morador` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_pets: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_pets` DISABLE KEYS */;
INSERT INTO `ap_pets` (`id`, `nomePet`, `tipo`, `id_morador`, `dataCadastro`) VALUES
	(1, 'Lupe', 'Cachorro', 1, '2022-03-30 11:32:35'),
	(2, 'Tina', 'Cachorro', 1, '2022-03-30 11:32:52'),
	(3, 'Zeus', 'Cachorro', 2, '2022-03-30 11:33:07');
/*!40000 ALTER TABLE `ap_pets` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_unidade
CREATE TABLE IF NOT EXISTS `ap_unidade` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_bloco` int(11) NOT NULL DEFAULT 0,
  `id_condominio` int(11) NOT NULL DEFAULT 0,
  `numeroUnidade` varchar(5) NOT NULL DEFAULT '',
  `metragem` float NOT NULL DEFAULT 0,
  `vagasDeGaragem` int(2) NOT NULL DEFAULT 0,
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  `dataModificado` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `chCondominioUnid` (`id_condominio`),
  KEY `chBlocoUnid` (`id_bloco`),
  CONSTRAINT `chBlocoUnid` FOREIGN KEY (`id_bloco`) REFERENCES `ap_bloco` (`id`),
  CONSTRAINT `chCondominioUnid` FOREIGN KEY (`id_condominio`) REFERENCES `ap_condominio` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_unidade: ~9 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_unidade` DISABLE KEYS */;
INSERT INTO `ap_unidade` (`id`, `id_bloco`, `id_condominio`, `numeroUnidade`, `metragem`, `vagasDeGaragem`, `dataCadastro`, `dataModificado`) VALUES
	(1, 1, 1, '705', 68, 2, '2022-03-29 14:23:40', '2022-03-29 14:23:42'),
	(2, 12, 1, '45', 72, 4, '2022-03-29 14:38:05', '2022-03-29 14:38:05'),
	(3, 2, 1, '55', 72, 3, '2022-03-29 14:39:27', NULL),
	(14, 7, 2, '406', 42, 1, '2022-04-04 08:22:43', NULL),
	(15, 12, 1, '705', 70, 2, '2022-04-04 09:05:38', NULL),
	(16, 4, 1, '306', 42, 1, '2022-04-04 10:32:46', NULL),
	(17, 14, 1, '104', 42, 1, '2022-04-04 10:35:03', NULL),
	(18, 7, 1, '406', 42, 1, '2022-04-06 11:54:44', NULL),
	(19, 7, 1, '406', 42, 1, '2022-04-06 11:56:24', NULL);
/*!40000 ALTER TABLE `ap_unidade` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.ap_usuario
CREATE TABLE IF NOT EXISTS `ap_usuario` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `nome` varchar(255) NOT NULL DEFAULT '',
  `usuario` varchar(50) NOT NULL DEFAULT '',
  `senha` varchar(255) NOT NULL DEFAULT '',
  `data` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.ap_usuario: ~11 rows (aproximadamente)
/*!40000 ALTER TABLE `ap_usuario` DISABLE KEYS */;
INSERT INTO `ap_usuario` (`id`, `nome`, `usuario`, `senha`, `data`) VALUES
	(10, 'reginaldo', 'reginaldo', '698dc19d489c4e4db73e28a713eab07b', '2022-04-07 17:06:46'),
	(11, 'reginaldo', 'reginaldo', '827ccb0eea8a706c4c34a16891f84e7b', '2022-04-10 17:12:45'),
	(12, 'teste', 'teste', '827ccb0eea8a706c4c34a16891f84e7b', '2022-04-10 17:16:11'),
	(13, 'yolo', 'yoll', '698dc19d489c4e4db73e28a713eab07b', '2022-04-11 20:50:38'),
	(15, 'outro', 'outro', '7e98b8a17c0aad30ba64d47b74e2a6c1', '2022-04-10 18:07:33'),
	(16, 'mais um', 'mais um', '7e98b8a17c0aad30ba64d47b74e2a6c1', '2022-04-10 18:09:08'),
	(17, 'oi', 'oi', 'a2e63ee01401aaeca78be023dfbb8c59', '2022-04-10 18:10:07'),
	(18, 'unico', 'unico', '0674272bac0715f803e382b5aa437e08', '2022-04-10 18:22:06'),
	(19, 'yes', 'yes', '2fb1c5cf58867b5bbc9a1b145a86f3a0', '2022-04-10 18:23:36'),
	(20, 'dois', 'dois', '1aabac6d068eef6a7bad3fdf50a05cc8', '2022-04-10 18:24:12'),
	(21, 'exclusivo', 'exclusivo', '6277e2a7446059985dc9bcf0a4ac1a8f', '2022-04-10 18:30:54');
/*!40000 ALTER TABLE `ap_usuario` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.lista_convidados
CREATE TABLE IF NOT EXISTS `lista_convidados` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_evento` int(11) NOT NULL DEFAULT 0,
  `id_unidade` int(11) NOT NULL DEFAULT 0,
  `nomeConvidado` varchar(255) NOT NULL DEFAULT '',
  `cpfConvidado` varchar(11) NOT NULL DEFAULT '',
  `celularConvidado` varchar(15) NOT NULL DEFAULT '',
  `dataCadastro` timestamp NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`),
  KEY `chConvidadoEvento` (`id_evento`),
  KEY `chConvidadoUnidade` (`id_unidade`),
  CONSTRAINT `chConvidadoEvento` FOREIGN KEY (`id_evento`) REFERENCES `reserva_salao_festas` (`id`),
  CONSTRAINT `chConvidadoUnidade` FOREIGN KEY (`id_unidade`) REFERENCES `ap_unidade` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.lista_convidados: ~24 rows (aproximadamente)
/*!40000 ALTER TABLE `lista_convidados` DISABLE KEYS */;
INSERT INTO `lista_convidados` (`id`, `id_evento`, `id_unidade`, `nomeConvidado`, `cpfConvidado`, `celularConvidado`, `dataCadastro`) VALUES
	(1, 1, 1, 'Pedinho', '35413136453', '677954654984', '2022-03-30 14:08:41'),
	(3, 1, 1, 'Zézinho', '35413136453', '677954654984', '2022-03-30 14:08:41'),
	(4, 1, 1, 'Luizinho', '35413136453', '677954654984', '2022-03-30 14:08:41'),
	(5, 1, 1, 'Garrincha', '35413136453', '677954654984', '2022-03-30 14:08:41'),
	(6, 1, 1, 'Pelé', '35413136453', '677954654984', '2022-03-30 14:08:41'),
	(7, 1, 1, 'Norberto', '35413136453', '677954654984', '2022-03-30 14:08:41'),
	(8, 1, 1, 'Charlie Brown', '35413136453', '677954654984', '2022-03-30 14:08:41'),
	(9, 3, 3, 'Molho', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(10, 3, 3, 'Gridnaldo', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(11, 3, 3, 'Francispy', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(12, 3, 3, 'Rodeio', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(13, 3, 3, 'Fluccas', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(14, 3, 3, 'MC Luiz', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(15, 3, 3, 'Doutor Carlos', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(16, 3, 3, 'Marcelino', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(25, 2, 2, 'Caio', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(26, 2, 2, 'Renan', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(27, 2, 2, 'Christian', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(28, 2, 2, 'Ricardo', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(29, 2, 2, 'Lucas', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(30, 2, 2, 'Gregori', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(31, 2, 2, 'Rodrigo', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(32, 2, 2, 'Gabriel', '78946436135', '484867498646', '2022-03-30 14:11:31'),
	(33, 2, 2, 'Rafael', '78946436135', '484867498646', '2022-03-30 14:11:31');
/*!40000 ALTER TABLE `lista_convidados` ENABLE KEYS */;

-- Copiando estrutura para tabela ap_controle.reserva_salao_festas
CREATE TABLE IF NOT EXISTS `reserva_salao_festas` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `id_unidade` int(11) NOT NULL DEFAULT 0,
  `tituloEvento` varchar(255) NOT NULL DEFAULT '',
  `dataHoraEvento` datetime NOT NULL,
  `dataCadastro` datetime NOT NULL DEFAULT current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;

-- Copiando dados para a tabela ap_controle.reserva_salao_festas: ~3 rows (aproximadamente)
/*!40000 ALTER TABLE `reserva_salao_festas` DISABLE KEYS */;
INSERT INTO `reserva_salao_festas` (`id`, `id_unidade`, `tituloEvento`, `dataHoraEvento`, `dataCadastro`) VALUES
	(1, 1, 'Festa de Aniversário', '2022-05-07 18:00:00', '2022-03-30 13:10:16'),
	(2, 2, 'Churras do Naldo', '2022-04-30 11:00:00', '2022-03-30 13:10:50'),
	(3, 3, 'Festa da Firma', '2022-05-22 12:00:00', '2022-03-30 13:12:02');
/*!40000 ALTER TABLE `reserva_salao_festas` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IFNULL(@OLD_FOREIGN_KEY_CHECKS, 1) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40111 SET SQL_NOTES=IFNULL(@OLD_SQL_NOTES, 1) */;
