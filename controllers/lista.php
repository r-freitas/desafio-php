<?php

// legivel($_GET);
//consultar lista de condomínios
$condominio = new Condominio();
$listCondominio = $condominio->getCondominio();

//consultar lista de moradores
$moradorOBJ = new Morador();
$moradorOBJ->pagination = 5;

if(isset($_GET['b'])){
    $filtro = array();
    foreach($_GET['b'] as $field=>$termo){
        switch($field){
            case 'termo1':
                $filtro['nome'] = $termo;
                break;
            case 'termo2':
                $filtro['id_condominio'] = $termo;
                break;
        }
    }
}

$moradorOBJ->buscar = $filtro;
$result = $moradorOBJ->getMoradores();

//paginação
$paginacao = (($moradorOBJ->pagination < $result['totalResults']) ? $moradorOBJ->renderPagination($result['qtPaginas']) : '');

?>