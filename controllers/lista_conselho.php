<?php
//instancia o objeto
 $conselho = new Conselho();
//lista condominios para o select da busca
 $listCondominio = $conselho->getCondominio();
 //define itens por pagina
 $conselho->pagination = 5;
//passa os termos da busca
 if(isset($_GET['b'])){
    $filtro = array();
    foreach($_GET['b'] as $field=>$termo){
        switch($field){
            case 'termo1':
                $filtro['nomeConselho'] = $termo;
                break;
            case 'termo2':
                $filtro['id_condominio'] = $termo;
                break;
        }
    }
}
$conselho->buscar = $filtro;
 //lista conselheiros cadastrados
 $result = $conselho->getConselho();
//renderiza paginacao
$paginacao = $conselho->renderPagination($result['qtPaginas']);
?>