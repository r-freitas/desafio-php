<?php
include "uteis.php";
$user = new Restrito();

if (!$user->acesso()) {
    header("Location: login.php?msg=Faca o login");
}

if ($_GET['page'] == 'logout') {
    if ($user->logout()) {
        header('Location: ' . $url_site . 'login.php');
    }
}

?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="<?= $url_site ?>css/bootstrap.min.css">
    <link rel="stylesheet" href="<?= $url_site ?>css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="shortcut icon" href="#">
    <title>Cadastro de Clientes</title>
    <style>
        main {
            min-height: 80vh;
        }
    </style>
</head>

<body>



    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
            <a class="navbar-brand" href="#"><img src="<?= $url_site ?>/img/Logo-site.png" width="100px"></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                    <li class="nav-item"><a class="nav-link text-white" href="<?= $url_site ?>">Início</a></li>
                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                            Administradoras
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="<?= $url_site ?>cadastro_administradora">Cadastro Administradora</a>
                            <a class="dropdown-item" href="<?= $url_site ?>lista_administradora">Listar Administradoras</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown ">
                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                            Moradores
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="<?= $url_site ?>cadastro">Cadastro de Morador</a>
                            <a class="dropdown-item" href="<?= $url_site ?>lista">Listar Moradores</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                            Condomínios
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="<?= $url_site ?>cadastro_condominio">Cadastro de Condomínio</a>
                            <a class="dropdown-item" href="<?= $url_site ?>lista_condominio">Listar Condomínios</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                            Blocos
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="<?= $url_site ?>cadastro_bloco">Cadastro de Bloco</a>
                            <a class="dropdown-item" href="<?= $url_site ?>lista_bloco">Listar Blocos</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                            Unidades
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="<?= $url_site ?>cadastro_unidade">Cadastro de Unidade</a>
                            <a class="dropdown-item" href="<?= $url_site ?>lista_unidade">Listar Unidades</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                            Conselho Fiscal
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="<?= $url_site ?>cadastro_conselho">Cadastro de Conselho</a>
                            <a class="dropdown-item" href="<?= $url_site ?>lista_conselho">Listar Conselho</a>
                        </div>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle text-white" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-expanded="false">
                            Usuários do Sistema
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                            <a class="dropdown-item" href="<?= $url_site ?>cadastro_usuario">Cadastro de Usuario</a>
                            <a class="dropdown-item" href="<?= $url_site ?>lista_usuario">Listar Usuários</a>
                        </div>
                    </li>
                </ul>
                <ul class="navbar-nav me-auto mb-2 mb-lg-0 ml-auto">
                    <li class="nav-item text-right"><a class="nav-link text-white" href="<?= $url_site ?>logout">Sair <i class="bi bi-box-arrow-right"></i></a></li>
                </ul>
            </div>
        </div>
    </nav>

    <main class="container mt-3 mb-5">
        <div class="row">
            <?php
            switch ($_GET['page']) {
                case '':
                case 'inicio':
                    require "controllers/inicio.php";
                    require "views/inicio.php";
                    break;
                default:
                    require 'controllers/' . $_GET['page'] . '.php';
                    require 'views/' . $_GET['page'] . '.php';
                    break;
            }
            ?>
        </div>
    </main>
    <footer class="container-fluid bg-dark text-white mt-5">
        <div class="row justify-content-around align-middle">
            <p class="align-self-center">Todos os direitos reservados</p>
            <div class="d-flex flex-column text-center">
                <p class=" mt-2">Fox Shadow - Entregando resultados</p>
            </div>
        </div>
    </footer>
    <script>
        var url_site = '<?= $url_site ?>';
    </script>
    <script src="<?= $url_site ?>js/jquery-3.6.0.min.js"></script>
    <script src="<?= $url_site ?>js/app.js?v=<?= rand(0, 9999) ?>"></script>
    <script src="<?= $url_site ?>js/bootstrap.bundle.min.js"></script>
</body>

</html>