$(function () {

    $('.formAdministradora').submit(function () {
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if (editar) {
            url = url_site+'api/editaAdministradora.php';
            urlRedir = url_site+'lista_administradora';

        } else {
            url = url_site+'api/cadastraAdministradora.php';
            urlRedir = url_site+'cadastro_administradora';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    })

    $('.tabelaAdministradoras').on('click', '.removerAdministradora', function () {
        var idRegistro = $(this).parent().parent().attr('data-id')
        $.ajax({
            url: url_site+'api/deletaAdministradora.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'lista_administradora');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })
    $('.formMorador').submit(function () {
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if (editar) {
            url = url_site+'api/editaMorador.php';
            urlRedir = url_site+'lista';

        } else {
            url = url_site+'api/cadastraMorador.php';
            urlRedir = url_site+'cadastro';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    })

    $('.tabelaMoradores').on('click', '.removerMorador', function () {
        var idRegistro = $(this).parent().parent().attr('data-id')
        $.ajax({
            url: url_site+'api/deletaMorador.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'lista');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('.formCondominio').submit(function () {
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if (editar) {
            url = url_site+'api/editaCondominio.php';
            urlRedir = url_site+'lista_condominio';
        } else {
            url = url_site+'api/cadastraCondominio.php';
            urlRedir = url_site+'cadastro_condominio';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    })

    $('.tabelaCondominios').on('click', '.removerCondominio', function () {
        var idRegistro = $(this).parent().parent().attr('data-id')
        $.ajax({
            url: url_site+'api/deletaCondominio.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'lista_condominio');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('.formBloco').submit(function () {
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if (editar) {
            url = url_site+'api/editaBloco.php';
            urlRedir = url_site+'lista_bloco';
        } else {
            url = url_site+'api/cadastraBloco.php';
            urlRedir = url_site+'cadastro_bloco';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    })

    $('.tabelaBlocos').on('click', '.removerBloco', function () {
        var idRegistro = $(this).parent().parent().attr('data-id')
        $.ajax({
            url: url_site+'api/deletaBloco.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'lista_bloco');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('.formUnidade').submit(function () {
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if (editar) {
            url = url_site+'api/editaUnidade.php';
            urlRedir = url_site+'lista_unidade';
        } else {
            url = url_site+'api/cadastraUnidade.php';
            urlRedir = url_site+'cadastro_unidade';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    })

    $('.tabelaUnidades').on('click', '.removerUnidade', function () {
        var idRegistro = $(this).parent().parent().attr('data-id')
        $.ajax({
            url: url_site+'api/deletaUnidade.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'lista_unidade');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('.formConselho').submit(function () {
        var editar = $(this).find('input[name="editar"]').val();
        var url;
        var urlRedir;
        if (editar) {
            url = url_site+'api/editaConselho.php';
            urlRedir = url_site+'lista_conselho';
        } else {
            console.log('cheguei aqui');
            url = url_site+'api/cadastraConselho.php';
            urlRedir = url_site+'cadastro_conselho';
        }

        $('.buttonEnviar').attr('disabled', 'disabled');

        $.ajax({
            url: url,
            dataType: 'json',
            type: 'POST',
            data: $(this).serialize(),
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                } else {
                    myAlert(data.status, data.msg, 'main', urlRedir);
                }
            }
        })
        return false;
    })

    $('.tabelaConselho').on('click', '.removerConselho', function () {
        var idRegistro = $(this).parent().parent().attr('data-id');
        $.ajax({
            url: url_site+'api/deletaConselho.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'lista_conselho');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('.formUsuario').submit(function(){
        var senha = $(this).find('input[name="u[senha]"]').val();
        var confirmaSenha = $(this).find('input[name="cSenha"]').val();
        if(senha == confirmaSenha){
            var editar = $(this).find('input[name="u[editar]"]').val();
            var url;
            var urlRedir;
            if(editar){
                url = url_site+'api/editaUsuario.php'
                urlRedir = url_site+'lista_usuario'
            }else{
                url = url_site+'api/cadastraUsuario.php'
                urlRedir = url_site+'cadastro_usuario'
            }
            $('.buttonEnviar').attr('disabled', 'disabled');
            
            $.ajax({
                url: url,
                dataType: 'json',
                type: 'POST',
                data: $(this).serialize(),
                success: function (data) {
                    if (data.status == 'success') {
                        myAlert(data.status, data.msg, 'main', urlRedir);
                    } else {
                        myAlert(data.status, data.msg, 'main', urlRedir);
                    }
                }
            })
        }else{
            myAlert('danger', 'As senhas informadas não são iguais.', 'form')
        }

        return false;

    })

    $('.tabelaUsuarios').on('click', '.removerUsuario', function () {
        console.log('estou aqui')
        var idRegistro = $(this).parent().parent().attr('data-id');
        console.log(idRegistro)
        $.ajax({
            url: url_site+'api/deletaUsuario.php',
            dataType: 'json',
            type: 'POST',
            data: { id: idRegistro },
            success: function (data) {
                if (data.status == 'success') {
                    myAlert(data.status, data.msg, 'main', url_site+'lista_usuario');
                } else {
                    myAlert(data.status, data.msg, 'main');
                }
            }
        })
        return false;
    })

    $('.fromCondominio').change(function () {
        var selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listBlocos.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado },
            success: function (data) {
                console.log(data.resultSet);
                selectPopulation('.fromBloco', data.resultSet, 'nomeBloco');
            }
        })

    })

    //chamar unidades após selecionar blocos
    $('.fromBloco').change(function () {
        var selecionado = $(this).val();

        $.ajax({
            url: url_site+'api/listUnidades.php',
            dataType: 'json',
            type: 'POST',
            data: { id: selecionado },
            success: function (data) {
                console.log(data.resultSet);
                selectPopulation('.fromUnidade', data.resultSet, 'numeroUnidade');
            }
        })

    })

    function selectPopulation(seletor, dados, field) {
        estrutura = '<option value="">Selecione...</option>';

        // if(dados.length > 1){
        for (let i = 0; i < dados.length; i++) {
            estrutura += '<option value="' + dados[i].id + '">' + dados[i][field] + '</option>';
        }
        // }else{
        //     estrutura += '<option value"'+dados.id+'">'+dados.nomeBloco+'</option>';
        // }
        $(seletor).html(estrutura);
    }

})

//controlador do filtro
    $('#filtro').submit(function(){
      var pagina = $('input[name="page"]').val();
      var termo1 = $('.termo1').val();
      var termo2 = $('.termo2').val();

      termo1 = (termo1) ? termo1 + '/' : '';
      termo2 = (termo2) ? termo2 + '/' : '';

      window.location.href = url_site+pagina+'/busca/'+termo1+termo2;

      return false;
    })

    $('.termo1, .termo2').on('keyup focusout change',function(){
        var termo1 = $('.termo1').val();
        var termo2 = $('.termo2').val();
        if(termo1 || termo2){
            $('button[type="submit"]').prop('disabled',false);
        }else{
            $('button[type="submit"]').prop('disabled',true);
        }
    })


function myAlert(tipo, mensagem, pai, url) {
    url = (url == undefined) ? url = '' : url = url;
    componente = '<div class="alert alert-' + tipo + '" role="alert">' + mensagem + '</div>';

    $(pai).prepend(componente);

    setTimeout(function () {
        $(pai).find('div.alert').remove();
        if (tipo == 'success' && url) {
            setTimeout(function () {
                window.location.href = url;
            }, 500)
        }
    }, 3000)

}