<?php
include "uteis.php";
?>

<!DOCTYPE html>
<html lang="pt-BR">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/app.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.8.1/font/bootstrap-icons.css">
    <link rel="shortcut icon" href="#">
    <title>Cadastro de Clientes</title>
</head>

<body>
    <main class="container mt-3 mb-5">
        <div class="row justify-content-center">

            <form action="<?= $url_site ?>controllers/restrito.php" method="post" class="col-sm-4 bg-dark rounded shadow p-4">
                <center><img class="mb-4 text-center" src="img/Logo-site.png" alt="" width="142"></center>
                <h1 class="h3 mb-3 fw-normal text-white">Login</h1>

                <div class="form-floating">
                    <input type="text" class="form-control" id="floatingInput" placeholder="usuario" name="usuario" required>
                    <label for="floatingInput" class="text-white">Usuário</label>
                </div>
                <div class="form-floating">
                    <input type="password" class="form-control" id="floatingPassword" placeholder="senha" name="senha" required>
                    <label for="floatingPassword" class="text-white">Senha</label>
                </div>

                <div class="checkbox mb-3">
                    <label class="text-white">
                        <input type="checkbox" value="remember-me"> Manter logado
                    </label>
                </div>
                <button class="w-100 btn btn-lg btn-warning" type="submit">Entrar</button>
            </form>
        </div>
    </main>

    <script src="js/jquery-3.6.0.min.js"></script>
    <script src="js/bootstrap.bundle.min.js"></script>
    <script src="js/app.js?v=<?= rand(0, 9999) ?>"></script>
    <?php
    if (isset($_GET['msg'])) {
    ?>
        <script type="text/javascript">
            $(function() {
                myAlert('danger', '<?= $_GET['msg'] ?>', 'main')
            })
        </script>
    <?php } ?>
</body>

</html>