<?php

    Class Morador extends Unidade{
        protected $id;

        function __construct(){

        }

        public function getMoradores($id = null){
            $qry = "SELECT 
            morador.id, morador.nome, morador.id_unidade, morador.id_bloco, morador.id_condominio, morador.cpf, morador.email, morador.telefone, morador.dataCadastro, morador.dataModificacao, condo.nomeCondominio
            FROM 
            ap_morador morador
            INNER JOIN ap_condominio condo ON morador.id_condominio = condo.id";
            $contaTermos = count($this->buscar);
            if($contaTermos > 0){
                
                $i = 0;
                foreach($this->buscar as $field=>$termo){
                    if($i ==0 && $termo!=null){
                        $qry = $qry.' WHERE ';
                        $i++;
                    }
                    
                    // return $qry;
                    // exit;
                    switch ($termo) {
                        case is_numeric($termo):
                            if(!empty($termo)){
                                $qry = $qry.$field.' = '.$termo.' AND ';
                            }
                            break;
                            default:
                            if(!empty($termo)){
                                $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                            }
                            break;
                    }
    
                }
                $qry = rtrim($qry, ' AND');
            }
            if($id){
                $qry .= ' WHERE morador.id ='.$id;
                $unique = true;
            }
            return $this->listarData($qry,$unique);
            legivel($qry);
            exit;
        }
        
        public function setMorador($dados){
            $values ='';
            $sql = 'INSERT INTO ap_morador (';
            foreach($dados as $ch=>$value){
                $sql .= '`'.$ch.'`, ';
                $values .= "'".$value."', ";
            }
            $sql = rtrim($sql, ', ');
            $sql .= ') VALUES('.rtrim($values, ', ').')';
            return $this->insertData($sql);
        }

        public function editMorador($dados){
            $sql = 'UPDATE ap_morador SET ';
            foreach($dados as $ch=>$value){
                if($ch != 'editar'){
                    $sql .= "`".$ch."` = '".$value."', ";
                }
            }
            $sql = rtrim($sql, ', ');
            $sql .= ' WHERE id='.$dados['editar'];
            return $this->updateData($sql);
        }

        public function deletaMorador($id){
            $sql = 'DELETE FROM ap_morador WHERE id ='.$id;
            return $this->deletar($sql);
        }

    }

?>