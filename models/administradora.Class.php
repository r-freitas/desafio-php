<?php

    Class Administradora extends Dao{
        protected $id;
        protected $nomeAdministradora;
        protected $cnpj;

        function __construct(){

        }

        public function getAdministradora($id = null){
            $qry = 'SELECT * FROM ap_administradora';
            $contaTermos = count($this->buscar);
            if ($contaTermos > 0) {
                $qry = $qry . ' WHERE nomeAdministradora LIKE "%' . $this->buscar . '%"';
            }
            if($id){
                $qry .= ' WHERE id = '.$id;
                $unique = true;
            }
            return $this->listarData($qry, $unique);
        }
        
        public function setAdministradora($dados){
            $values ='';
            $sql = 'INSERT INTO ap_administradora (';
            foreach($dados as $ch=>$value){
                $sql .= '`'.$ch.'`, ';
                $values .= "'".$value."', ";
            }
            $sql = rtrim($sql, ', ');
            $sql .= ') VALUES('.rtrim($values, ', ').')';
            return $this->insertData($sql);
        }

        public function editAdministradora($dados){
            $sql = 'UPDATE ap_administradora SET ';
            foreach($dados as $ch=>$value){
                if($ch != 'editar'){
                    $sql .= "`".$ch."` = '".$value."', ";
                }
            }
            $sql = rtrim($sql, ', ');
            $sql .= ' WHERE id='.$dados['editar'];
            return $this->updateData($sql);
        }

        public function deletaAdministradora($id){
            $sql = 'DELETE FROM ap_administradora WHERE id ='.$id;
            return $this->deletar($sql);
        }

    }

?>