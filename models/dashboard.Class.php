<?php
    Class Dashboard extends Dao{

        function __construct(){
            
        }

        public function contaMorador(){
            $sql = 'SELECT condo.nomeCondominio, COUNT(morador.id) AS total FROM ap_morador morador RIGHT JOIN ap_condominio condo ON condo.id = morador.id_condominio GROUP BY id_condominio'; 
            return $this->listarData($sql);
        }

        public function contaUsuarios(){
            $sql = 'SELECT
            COUNT(ap_administradora.id) AS totalAdm,
            (SELECT COUNT(id) FROM ap_condominio) AS totalCond,
            (SELECT COUNT(id) FROM ap_bloco) AS totalBlocos,
            (SELECT COUNT(id) FROM ap_unidade) AS totalUnidades,
            (SELECT COUNT(id) FROM ap_morador) AS totalMoradores,
            (SELECT COUNT(id) FROM ap_pets) AS totalPets
            FROM ap_administradora';
            return $this->listarData($sql, true);
        }

        public function cincoUltimasAdm(){
            $qry = 'SELECT ap_administradora.nomeAdministradora FROM ap_administradora ORDER BY ap_administradora.dataCadastro DESC LIMIT 5';
            return $this->listarData($qry);
        }

    }



?>