<?php
    Class Usuario extends Dao{

        function __construct(){
            
        }

        function getUsuario($id = null){
            $qry = 'SELECT usuario.id, usuario.nome, usuario.usuario, usuario.data FROM ap_usuario usuario';
            $contaTermos = count($this->buscar);
            if ($contaTermos > 0) {
                $qry = $qry . ' WHERE usuario LIKE "%' . $this->buscar . '%"';
            }
            if($id){
                $qry .= ' WHERE usuario.id = '.$id;
                $unique = true;
            }
            return $this->listarData($qry, $unique);
        }

        function verificaUsuario($usuario){
            $qry = "SELECT usuario.id FROM ap_usuario usuario WHERE usuario.usuario = '".$usuario."'";
            return $this->listarData($qry);
        }

        function setUsuario($dados){
            $values = '';
            $sql = 'INSERT INTO ap_usuario (';
            foreach($dados as $ch=>$value){
                $sql .= '`'.$ch.'`, ';
                $values .= "'".$value."', ";
            }
            $sql = rtrim($sql, ', ');
            $sql .= ') VALUES('.rtrim($values, ', ').')';
            return $this->insertData($sql);
        }

        function editUsuario($dados){
            $sql = 'UPDATE ap_usuario SET ';
            foreach($dados as $ch=>$value){
                if($ch != 'editar'){
                    $sql .= "`".$ch."` = '".$value."', ";
                }
            }
            $sql = rtrim($sql, ', ');
            $sql .= ' WHERE id='.$dados['editar'];
            return $this->updateData($sql);
        }

        function deletaUsuario($id){
            $sql = 'DELETE FROM ap_usuario WHERE id ='.$id;
            return $this->deletar($sql);
        }
    }

?>