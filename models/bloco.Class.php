<?php
    Class Bloco extends Condominio{
        protected $nome;
        protected $andares;
        protected $qtdUnidadesAndar;

        function __construct(){
            
        }

        function getBlocos($id = null){
            $qry = 'SELECT bloco.id, bloco.id_condominio, bloco.nomeBloco, bloco.numeroAndares, bloco.unidadesPAndar, bloco.dataCadastro, bloco.dataModificado, condo.nomeCondominio FROM ap_bloco bloco INNER JOIN ap_condominio condo ON bloco.id_condominio = condo.id';
            $contaTermos = count($this->buscar);
            if ($contaTermos > 0) {
                $qry = $qry . ' WHERE bloco.id_condominio = '. $this->buscar ;
            }
            if($id){
                $qry .= ' WHERE bloco.id = '.$id;
                $unique = true;
            }
            return $this->listarData($qry, $unique, 1);
        }

        function getBlocoFromCond($id){
            $qry = 'SELECT id, nomeBloco FROM ap_bloco WHERE id_condominio ='.$id;
            return $this->listarData($qry);
        }

        function setBloco($dados){
            $values = '';
            $sql = 'INSERT INTO ap_bloco (';
            foreach($dados as $ch=>$value){
                $sql.='`'.$ch.'`, ';
                $values.="'".$value."', ";
            }
            $sql = rtrim($sql,', ');
            $sql .= ') VALUES('.rtrim($values, ', ').')';
            return $this->insertData($sql);
        }

        function editBloco($dados){
            $sql = 'UPDATE ap_bloco SET ';
            foreach($dados as $ch=>$value){
                if($ch != 'editar'){
                    $sql .= "`" . $ch ."` = '".$value."',";
                }
            }
            $sql = rtrim($sql,', ');
            $sql .= ' WHERE id='.$dados['editar'];
            return $this->updateData($sql);
        }

        function deletaBloco($id){
            $sql = 'DELETE FROM ap_bloco WHERE id ='.$id;
            return $this->deletar($sql);
        }

    }
?>