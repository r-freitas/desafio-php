<?php

    Class Conselho extends Condominio{
        protected $idConselho;
        protected $nomeConselho;
        protected $telefoneConselho;
        protected $emailConselho;

        function __construct(){

        }

        public function getConselho($id = null){
            $qry = 'SELECT conselho.id, conselho.id_condominio, condo.nomeCondominio, conselho.funcao, conselho.nomeConselho, conselho.cpf, conselho.email, conselho.telefone, conselho.dataCadastro, conselho.dataModificado FROM ap_conselho conselho INNER JOIN ap_condominio condo ON condo.id = conselho.id_condominio';
            $contaTermos = count($this->buscar);
            if($contaTermos > 0){
                
                $i = 0;
                foreach($this->buscar as $field=>$termo){
                    if($i ==0 && $termo!=null){
                        $qry = $qry.' WHERE ';
                        $i++;
                    }
                    
                    // return $qry;
                    // exit;
                    switch ($termo) {
                        case is_numeric($termo):
                            if(!empty($termo)){
                                $qry = $qry.$field.' = '.$termo.' AND ';
                            }
                            break;
                            default:
                            if(!empty($termo)){
                                $qry = $qry.$field.' LIKE "%'.$termo.'%" AND ';
                            }
                            break;
                    }
    
                }
                $qry = rtrim($qry, ' AND');
            }
            if($id){
                $qry .= ' WHERE conselho.id = '.$id;
                $unique = true;
            }
            return $this->listarData($qry, $unique);
        }
        
        public function setConselho($dados){
            $values ='';
            $sql = 'INSERT INTO ap_conselho (';
            foreach($dados as $ch=>$value){
                $sql .= '`'.$ch.'`, ';
                $values .= "'".$value."', ";
            }
            $sql = rtrim($sql, ', ');
            $sql .= ') VALUES('.rtrim($values, ', ').')';
            return $this->insertData($sql);
        }

        public function editConselho($dados){
            $sql = 'UPDATE ap_conselho SET ';
            foreach($dados as $ch=>$value){
                if($ch != 'editar'){
                    $sql .= "`".$ch."` = '".$value."', ";
                }
            }
            $sql = rtrim($sql, ', ');
            $sql .= ' WHERE id='.$dados['editar'];
            return $this->updateData($sql);
        }

        public function deletaConselho($id){
            $sql = 'DELETE FROM ap_conselho WHERE id ='.$id;
            return $this->deletar($sql);
        }

    }

?>