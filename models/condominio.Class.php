<?php
class Condominio extends Dao
{
    protected $qtdBlocos;
    protected $nomeCondominio;
    protected $sindico;
    protected $enderecoCondominio = array();
    protected $dados = array();

    function __construct()
    {
    }

    function getCondominio($id = null)
    {
        $qry = "SELECT condo.id, condo.id_administradora, condo.nomeCondominio, condo.qtBlocos, condo.cep, condo.logradouro, condo.numero, condo.bairro, condo.cidade, condo.estado, condo.dataCadastro, condo.dataModificado, conselho.nomeConselho, conselho.funcao
             FROM ap_condominio condo LEFT JOIN ap_conselho conselho ON condo.id = conselho.id_condominio AND conselho.funcao = 'Sindico'";
        $contaTermos = count($this->buscar);
        if ($contaTermos > 0) {
            $qry = $qry . ' WHERE condo.nomeCondominio LIKE "%' . $this->buscar . '%"';
        }

        if ($id) {
            $qry .= ' WHERE condo.id = ' . $id;
            $unique = true;
        }
        return $this->listarData($qry, $unique);
    }

    function setCondominio($dados)
    {
        $values = '';
        $sql = 'INSERT INTO ap_condominio (';
        foreach ($dados as $ch => $value) {
            $sql .= '`' . $ch . '`, ';
            $values .= "'" . $value . "', ";
        }
        $sql = rtrim($sql, ', ');
        $sql .= ') VALUES(' . rtrim($values, ', ') . ')';
        return $this->insertData($sql);
    }

    function editCondominio($dados)
    {
        $sql = 'UPDATE ap_condominio SET ';
        foreach ($dados as $ch => $value) {
            if ($ch != 'editar') {
                $sql .= "`" . $ch . "` = '" . $value . "',";
            }
        }
        $sql = rtrim($sql, ', ');
        $sql .= ' WHERE id=' . $dados['editar'];
        return $this->updateData($sql);
    }

    function deletaCondominio($id)
    {
        $sql = 'DELETE FROM ap_condominio WHERE id=' . $id;
        return $this->deletar($sql);
    }
}
