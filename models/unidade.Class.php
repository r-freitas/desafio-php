<?php
    Class Unidade extends Bloco{
        protected $numeroUnidade;
        protected $metragem;
        protected $vagasDeGaragem;

        function __construct(){
            
        }

        function getUnidades($id = null){
            $qry = 'SELECT unidade.id, unidade.numeroUnidade, unidade.metragem, unidade.vagasDeGaragem, unidade.id_bloco, unidade.id_condominio, unidade.dataCadastro, unidade.dataModificado, bloco.nomeBloco, condo.nomeCondominio FROM ap_unidade unidade INNER JOIN ap_bloco bloco ON unidade.id_bloco = bloco.id INNER JOIN ap_condominio condo ON unidade.id_condominio = condo.id';
            $contaTermos = count($this->buscar);
            if ($contaTermos > 0) {
                $qry = $qry . ' WHERE unidade.id_condominio = '. $this->buscar ;
            }
            if($id){
                $qry .= ' WHERE unidade.id = '.$id;
                $unique = true;
            }
            return $this->listarData($qry, $unique);
        }
        
        function getUnidadeFromBloco($id){
            $qry = 'SELECT id, numeroUnidade FROM ap_unidade WHERE id_bloco ='.$id;
            return $this->listarData($qry);
        }

        function setUnidade($dados){
            $values = '';
            $sql = 'INSERT INTO ap_unidade(';
            foreach($dados as $ch=>$value){
                $sql .= '`'.$ch.'`, ';
                $values .= "'".$value."', ";
            }
            $sql = rtrim($sql,', ');
            $values = rtrim($values,', ');
            $sql .= ') VALUES('.$values.')';
            return $this->insertData($sql);
        }

        function editUnidade($dados){
            $sql = 'UPDATE ap_unidade SET ';
            foreach($dados as $ch=>$value){
                if($ch != 'editar'){
                    $sql .= "`".$ch."` = '".$value."', ";
                }
            }
            $sql = rtrim($sql, ', ');
            $sql .= ' WHERE id='.$dados['editar'];
            return $this->updateData($sql);
        }

        function deletaUnidade($id){
            $sql = 'DELETE FROM ap_unidade WHERE id ='.$id;
            return $this->deletar($sql);
        }

    }
?>