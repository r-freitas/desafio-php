<form class="form-inline mt-5 my-lg-0" method="GET" id="filtro">
    <input type="hidden" name="page" value="lista_unidade">
    <select name="b[id_condominio]" class="custom-select col-sm-4 mr-sm-2 termo2 mb-3">
        <option value="">Condomínio</option>
        <?php
        foreach ($listCondominio['resultSet'] as $condominios) { ?>
            <option value="<?= $condominios['id'] ?>"><?= $condominios['nomeCondominio'] ?></option>
        <?php } ?>
    </select>
    <button class="btn btn-dark col-sm-2 mr-2 ml-2 mb-3" type="submit" disabled>Buscar</button>
    <a class="btn btn-dark col-sm-2 mb-3 mr-2 ml-2" type="reset" href="<?= $url_site ?>lista_unidade">Limpar</a>
</form>
<div class="table-responsive">
    <table class="table mb-5 tabelaUnidades">
        <thead>
            <tr>
                <th scope="col">Número Unidade</th>
                <th scope="col">Metragem</th>
                <th scope="col">Vagas de Garagem</th>
                <th scope="col">Condominio</th>
                <th scope="col">Bloco</th>
                <th scope="col">Data Cad.</th>
                <th scope="col">Data Mod.</th>
                <th><a href="index.php?page=cadastro_unidade"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($result['resultSet'] as $valor){ 
            ?>
                <tr data-id="<?php echo $valor['id']; ?>" class="unidade">
                    <td><?php echo $valor['numeroUnidade'] ?></td>
                    <td><?php echo $valor['metragem'] ?></td>
                    <td><?php echo $valor['vagasDeGaragem'] ?></td>
                    <td><?php echo $valor['nomeCondominio'] ?></td>
                    <td><?php echo $valor['nomeBloco'] ?></td>
                    <td><?php echo dateFormat($valor['dataCadastro']) ?></td>
                    <td><?php echo dateFormat($valor['dataModificado']) ?></td>
                    <td><a class="p-1 removerUnidade" href="#"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1" href="<?= $url_site ?>cadastro_unidade/<?php echo $valor['id']; ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a><td>
                </tr>
    
            <?php } ?> 
            <tr>
                <td colspan="7">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: ' . ($result['totalResults']<10? '0'. $result['totalResults'] : $result['totalResults']) ?></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="col-sm-12">
<?= $paginacao; ?>
</div>