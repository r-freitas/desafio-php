    <h5>Cadastro de Conselho</h5>
    <form action="" method="post" class="col-12 mt-3 formConselho">
        <div class="form-group">
            <label for="condominio">Selecione o Condomínio</label>
            <select class="custom-select" name="id_condominio">
                <option value="">Condominio</option>
                <?php 
                foreach($resultado['resultSet'] as $opcaoCond){ ?>
                        <option value="<?= $opcaoCond['id'] ?>" <?= ($opcaoCond['id'] == $popular['id_condominio'] ? 'selected' : '') ?>><?= $opcaoCond['nomeCondominio'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="funcao">Função</label>
            <select id="funcao" name="funcao" class="custom-select">
                <option value="Síndico">Síndico</option>
                <option value="Subsíndico">Subsíndico</option>
                <option value="Conselheiro">Conselheiro</option>
            </select>
        </div>
        <div class="form-group">
            <label for="nomeConselho">Nome</label>
            <input type="text" class="form-control" id="nomeConselho" name="nomeConselho" value="<?php echo $popular['nomeConselho']; ?>" required>
        </div>
        <div class="form-group">
            <label for="cpf">CPF</label>
            <input type="text" class="form-control" id="cpf" name="cpf" value="<?php echo $popular['cpf']; ?>" required>
        </div>
        <div class="form-group">
            <label for="email">Email</label>
            <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="<?php echo $popular['email']; ?>" required>
        </div>
        <div class="form-group">
            <label for="cpf">Telefone</label>
            <input type="text" class="form-control" id="telefone" name="telefone" value="<?php echo $popular['telefone']; ?>">
        </div>
        <?php if($_GET['id']){ ?>
            <input type="hidden" name="editar" value="<?php echo $_GET['id'] ?>">
        <?php } ?>
            <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </form>
