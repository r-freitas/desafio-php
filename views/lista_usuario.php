<form class="form-inline mt-5 my-lg-0" method="GET" id="filtro">
    <input type="hidden" name="page" value="lista_usuario">
    <input type="text" class="form-control col-sm-5 mr-sm-2 termo1 mb-3" placeholder="Nome Usuario" aria-label="Search" name="b[nome]">
    <button class="btn btn-dark col-sm-2 mr-2 ml-2 mb-3" type="submit" disabled>Buscar</button>
    <a class="btn btn-dark col-sm-2 mb-3 mr-2 ml-2" type="reset" href="<?= $url_site ?>lista_usuario">Limpar</a>
</form>
<div class="table-responsive">
    <table class="table mb-5 tabelaUsuarios">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Usuario</th>
                <th scope="col">Data Cad.</th>
                <th><a href="index.php?page=cadastro_usuario"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($result['resultSet'] as $usuario){ ?>
                <tr data-id="<?php echo $usuario['id']; ?>" class="usuario">
                    <td><?php echo $usuario['nome'] ?></td>
                    <td><?php echo $usuario['usuario'] ?></td>
                    <td><?php echo dateFormat($usuario['data']) ?></td>
                    <td><a class="p-1 removerUsuario" href="#"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1" href="<?= $url_site ?>cadastro_usuario/<?php echo $usuario['id']; ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a><td>
                </tr>
    
            <?php } ?> 
            <tr>
                <td colspan="3">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: ' . ($result['totalResults']<10? '0'. $result['totalResults'] : $result['totalResults'])?></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="col-sm-12">
<?= $paginacao; ?>
</div>