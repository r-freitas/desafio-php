<form class="form-inline mt-5 my-lg-0" method="GET" id="filtro">
    <input type="hidden" name="page" value="lista_conselho">
    <input type="text" class="form-control col-sm-3 mr-sm-2 termo1 mb-3" placeholder="Nome Morador" aria-label="Search" name="b[nome]">
    <select name="b[id_condominio]" class="custom-select col-sm-3 mr-sm-2 termo2 mb-3">
        <option value="">Condomínio</option>
            <?php
            foreach($listCondominio['resultSet'] as $condominios){ ?>
                <option value="<?= $condominios['id'] ?>"><?= $condominios['nomeCondominio'] ?></option>
            <?php } ?>
    </select>
    <button class="btn btn-dark col-sm-2 mr-2 ml-2 mb-3" type="submit" disabled>Buscar</button>
    <a class="btn btn-dark col-sm-2 mb-3 mr-2 ml-2" type="reset" href="<?= $url_site ?>lista_conselho">Limpar</a>
</form>
<div class="table-responsive">
    <table class="table mb-5 tabelaConselho">
        <thead>
            <tr>
                <th scope="col">Condomínio</th>
                <th scope="col">Função</th>
                <th scope="col">Nome</th>
                <th scope="col">CPF</th>
                <th scope="col">E-mail</th>
                <th scope="col">Telefone</th>
                <th scope="col">Data Cad.</th>
                <th scope="col">Data Mod.</th>
                <th><a href="index.php?page=cadastro_conselho"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($result['resultSet'] as $valor){ 
            ?>
                <tr data-id="<?php echo $valor['id'] ?>" class="conselho">
                    <td><?php echo $valor['nomeCondominio'] ?></td>
                    <td><?php echo $valor['funcao'] ?></td>
                    <td><?php echo $valor['nomeConselho'] ?></td>
                    <td><?php echo $valor['cpf'] ?></td>
                    <td><?php echo $valor['email'] ?></td>
                    <td><?php echo $valor['telefone'] ?></td>
                    <td><?php echo dateFormat($valor['dataCadastro']) ?></td>
                    <td><?php echo dateFormat($valor['dataModificado']) ?></td>
                    <td><a class="p-1 removerConselho" href="#"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1" href="<?= $url_site ?>cadastro_conselho/<?php echo $valor['id']; ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a><td>
                </tr>
    
            <?php } ?> 
            <tr>
                <td colspan="7">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: ' . ($result['totalResults']<10? '0'. $result['totalResults'] : $result['totalResults'])?></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="col-sm-12">
<?= $paginacao; ?>
</div>