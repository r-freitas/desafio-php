<form class="form-inline mt-5 my-lg-0" method="GET" id="filtro">
    <input type="hidden" name="page" value="lista_bloco">
    <select name="b[id_condominio]" class="custom-select col-sm-4 mr-sm-2 termo2 mb-3">
        <option value="">Condomínio</option>
            <?php
            foreach($listCondominio['resultSet'] as $condominios){ ?>
                <option value="<?= $condominios['id'] ?>"><?= $condominios['nomeCondominio'] ?></option>
            <?php } ?>
    </select>
    <button class="btn btn-dark col-sm-2 mr-2 ml-2 mb-3" type="submit" disabled>Buscar</button>
    <a class="btn btn-dark col-sm-2 mb-3 mr-2 ml-2" type="reset" href="<?= $url_site ?>lista_bloco">Limpar</a>
</form>
<div class="table-responsive">
    <table class="table mb-5 tabelaBlocos">
        <thead>
            <tr>
                <th scope="col">Nome bloco</th>
                <th scope="col">N° Andares</th>
                <th scope="col">Unidades Por Andar</th>
                <th scope="col">Condomínio</th>
                <th scope="col">Data Cad.</th>
                <th scope="col">Data Mod.</th>
                <th><a href="index.php?page=cadastro_bloco"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($result['resultSet'] as $bloco){ ?>
                <tr data-id="<?php echo $bloco['id']; ?>" class="bloco">
                    <td><?php echo $bloco['nomeBloco'] ?></td>
                    <td><?php echo $bloco['numeroAndares'] ?></td>
                    <td><?php echo $bloco['unidadesPAndar'] ?></td>
                    <td><?php echo $bloco['nomeCondominio'] ?></td>
                    <td><?php echo dateFormat($bloco['dataCadastro']) ?></td>
                    <td><?php echo dateFormat($bloco['dataModificado']) ?></td>
                    <td><a class="p-1 removerBloco" href="#"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1" href="<?= $url_site ?>cadastro_bloco/<?php echo $bloco['id']; ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a><td>
                </tr>
    
            <?php } ?> 
            <tr>
                <td colspan="6">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: ' . ($result['totalResults']<10? '0'. $result['totalResults'] : $result['totalResults'])?></td>
            </tr>
        </tbody>
    </table>
</div>

<div class="col-sm-12">
<?= $paginacao; ?>
</div>