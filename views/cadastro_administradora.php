    <h5>Cadastro de Administradora</h5>
    <form action="" method="post" class="col-12 mt-3 formAdministradora">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nomeAdministradora" name="nomeAdministradora" value="<?= $popular['nomeAdministradora'] ?>" required>
        </div>
        <div class="form-group">
            <label for="cpf">CNPJ</label>
            <input type="text" class="form-control" id="cnpj" name="cnpj" value="<?= $popular['cnpj']; ?>" required>
        </div>
        <?php if($_GET['id']){ ?>
            <input type="hidden" name="editar" value="<?= $_GET['id'] ?>">
        <?php } ?>
            <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </form>
