<h5>Cadastro de Unidade</h5>
<form action="" method="post" class="col-12 mt-3 formUnidade">
    <div class="form-group">
        <label for="nome">Condomínio</label>
        <select class="custom-select fromCondominio" name="id_condominio">
            <option value="">Condominio</option>
            <?php foreach ($condominioResult['resultSet'] as $opcaoCond) {
                echo '<option value="' . $opcaoCond['id'] . '" ' . ($popular['id_condominio'] == $opcaoCond['id'] ? 'selected' : '') . '>' . $opcaoCond['nomeCondominio'] . '</option>';
            } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="nome">Bloco</label>
        <select class="custom-select fromBloco" name="id_bloco">
            <?php if ($_GET['id']) {
                foreach ($blocos['resultSet'] as $bloco) {
            ?>
                    <option value="<?= $bloco['id'] ?>" <?= ($bloco['id'] == $popular['id_bloco'] ? 'selected' : '') ?>><?= $bloco['nomeBloco'] ?></option>
            <?php }
            } ?>
        </select>
    </div>
    <input type="hidden" value="1" name="id_condominio">
    <div class="form-group">
        <label for="numeroUnidade">Número da unidade</label>
        <input type="text" class="form-control" id="numeroUnidade" name="numeroUnidade" value="<?= $popular['numeroUnidade'] ?>" required>
    </div>
    <div class="form-group">
        <label for="metragem">Metragem da unidade</label>
        <input type="text" class="form-control" id="metragem" name="metragem" value="<?php echo $popular['metragem']; ?>" required>
    </div>
    <div class="form-group">
        <label for="vagasDeGaragem">Vagas de garagem</label>
        <input type="text" class="form-control" id="vagasDeGaragem" name="vagasDeGaragem" value="<?php echo $popular['vagasDeGaragem']; ?>" required>
    </div>
    <?php if ($_GET['id']) { ?>
        <input type="hidden" name="editar" value="<?php echo $_GET['id'] ?>">
    <?php } ?>
    <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
</form>