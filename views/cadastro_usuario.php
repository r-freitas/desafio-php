
    <h5>Cadastro de Usuários do Sistema</h5>
    <form action="" method="post" class="col-12 mt-3 formUsuario">
        <div class="form-group">
            <label for="nome">Nome</label>
            <input type="text" class="form-control" id="nome" name="u[nome]" value="<?= $popular['nome'] ?>" required>
        </div>
        <div class="form-group">
            <label for="usuario">Usuário</label>
            <input type="text" class="form-control" id="usuario" name="u[usuario]" value="<?= $popular['usuario'] ?>" required>
        </div>
        <div class="form-group">
            <label for="senha">Senha</label>
            <input type="password" class="form-control" id="senha" name="u[senha]" required>
        </div>
        <div class="form-group">
            <label for="cSenha">Confirmar Senha</label>
            <input type="password" class="form-control" id="cSenha" name="cSenha" required>
        </div>
        <?php if($_GET['id']){ ?>
            <input type="hidden" name="u[editar]" value="<?php echo $_GET['id'] ?>">
        <?php } ?>
            <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </form>
