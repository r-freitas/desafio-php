<div class="row justify-content-between">
    <h5 class="col-12">Seja bem-vindo <?= $nome[0]; ?>, escolha a opção na barra de navegação.</h5>
    
    <div class="jumbotron col-lg-6 col-md-5">
        <h1 class="display-5 mb-4">Moradores por Condomínio:</h1>
        <ul class="list-group">
            <?php
            foreach ($moradores['resultSet'] as $valorcond) { ?>
                <li class="list-group-item">
                    <?php
                    echo $valorcond['nomeCondominio'] . ': <span class="badge badge-warning">' . $valorcond['total'].'</span>';
                    ?>
                </li>
            <?php } ?>
        </ul>
    </div>
    <div class="jumbotron col-lg-5 col-md-6">
        <h1 class="display-5 mb-4">Administradoras:</h1>
        <ul class="list-group">
            <?php
            foreach ($adms['resultSet'] as $valoradm) { ?>
                <li class="list-group-item">
                    <?php
                    echo $valoradm['nomeAdministradora'];
                    ?>
                </li>
            <?php } ?>
        </ul>
        <small>Últimas 5 cadastradas.</small>
    </div>
    
    <div class="jumbotron col-12 mb-5">
        <div class="container mb-5">
            <center><h1 class="display-5 mb-5">Total de Usuários do Sistema:</h1></center>
            <div class="row">
                <div class="col-lg-4 col-md-6 text-center mb-3">
                    <h1><span class="badge badge-warning"><?= $contaUsuarios['resultSet']['totalAdm'] ?></span></h1>
                    <h1>Administradoras</h1>
                </div>
                <div class="col-lg-4 col-md-6 text-center mb-3">
                    <h1><span class="badge badge-warning"><?= $contaUsuarios['resultSet']['totalCond'] ?></span></h1>
                    <h1>Condomínios</h1>
                </div>
                <div class="col-lg-4 col-md-6 text-center mb-3">
                    <h1><span class="badge badge-warning"><?= $contaUsuarios['resultSet']['totalBlocos'] ?></span></h1>
                    <h1>Blocos</h1>
                </div>
                <div class="col-lg-4 col-md-6 text-center mb-3">
                    <h1><span class="badge badge-warning"><?= $contaUsuarios['resultSet']['totalUnidades'] ?></span></h1>
                    <h1>Unidades</h1>
                </div>
                <div class="col-lg-4 col-md-6 text-center mb-3">
                    <h1><span class="badge badge-warning"><?= $contaUsuarios['resultSet']['totalMoradores'] ?></span></h1>
                    <h1>Moradores</h1>
                </div>
                <div class="col-lg-4 col-md-6 text-center mb-3">
                    <h1><span class="badge badge-warning"><?= $contaUsuarios['resultSet']['totalPets'] ?></span></h1>
                    <h1>Pets</h1>
                </div>
            </div>
        </div>
    </div>
</div>



