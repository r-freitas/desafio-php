<h5>Cadastro de Morador</h5>
<form action="" method="post" class="col-12 mt-3 formMorador mb-5">
    <div class="form-group">
        <label for="nome">Condomínio</label>
        <select class="custom-select fromCondominio" name="id_condominio">
            <option value="">Condominio</option>
            <?php foreach ($condominioResult['resultSet'] as $opcaoCond) {
                echo '<option value="' . $opcaoCond['id'] . '" ' . ($popular['id_condominio'] == $opcaoCond['id'] ? 'selected' : '') . '>' . $opcaoCond['nomeCondominio'] . '</option>';
            } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="nome">Bloco</label>
        <select class="custom-select fromBloco" name="id_bloco">
            <?php if ($_GET['id']) {
                foreach ($blocos['resultSet'] as $bloco) {
            ?>
                    <option value="<?= $bloco['id'] ?>" <?= ($bloco['id'] == $popular['id_bloco'] ? 'selected' : '') ?>><?= $bloco['nomeBloco'] ?></option>
            <?php }
            } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="nome">Unidade</label>
        <select class="custom-select fromUnidade" name="id_unidade">
            <?php if ($_GET['id']) {
                foreach ($unidades['resultSet'] as $unidade) {
            ?>
                    <option value="<?= $unidade['id'] ?>" <?= ($unidade['id'] == $popular['id_unidade'] ? 'selected' : '') ?>><?= $unidade['numeroUnidade'] ?></option>
            <?php }
            } ?>
        </select>
    </div>
    <div class="form-group">
        <label for="nome">Nome</label>
        <input type="text" class="form-control" id="nome" name="nome" value="<?= $popular['nome'] ?>" required>
    </div>
    <div class="form-group">
        <label for="cpf">CPF</label>
        <input type="text" class="form-control" id="cpf" name="cpf" value="<?php echo $popular['cpf']; ?>" required>
    </div>
    <div class="form-group">
        <label for="email">Email</label>
        <input type="email" class="form-control" id="email" name="email" aria-describedby="emailHelp" value="<?php echo $popular['email']; ?>" required>
    </div>
    <div class="form-group">
        <label for="cpf">Telefone</label>
        <input type="text" class="form-control" id="telefone" name="telefone" value="<?php echo $popular['telefone']; ?>">
    </div>
    <?php if ($_GET['id']) { ?>
        <input type="hidden" name="editar" value="<?php echo $_GET['id'] ?>">
    <?php } ?>
    <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
</form>