
    <h5>Cadastro de Bloco</h5>
    <form action="" method="post" class="col-12 mt-3 formBloco">
        <div class="form-group">
            <label for="condominio">Condomínio</label>
            <select class="custom-select" name="id_condominio">
                <option value="">Condominio</option>
                <?php 
                foreach($resultado['resultSet'] as $opcaoCond){ ?>
                        <option value="<?= $opcaoCond['id'] ?>" <?= ($opcaoCond['id'] == $popular['id_condominio'] ? 'selected' : '') ?>><?= $opcaoCond['nomeCondominio'] ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="form-group">
            <label for="nomeBloco">Nome do Bloco</label>
            <input type="text" class="form-control" id="nomeBloco" name="nomeBloco" value="<?= $popular['nomeBloco'] ?>" required>
        </div>
        <div class="form-group">
            <label for="numeroAndares">Número de Andares</label>
            <input type="text" class="form-control" id="numeroAndares" name="numeroAndares" value="<?php echo $popular['numeroAndares']; ?>" required>
        </div>
        <div class="form-group">
            <label for="unidadesPAndar">Unidades por Andar</label>
            <input type="text" class="form-control" id="unidadesPAndar" name="unidadesPAndar" value="<?php echo $popular['unidadesPAndar']; ?>" required>
        </div>
        <?php if($_GET['id']){ ?>
            <input type="hidden" name="editar" value="<?php echo $_GET['id'] ?>">
        <?php } ?>
            <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </form>
