<form class="form-inline mt-5 my-lg-0" method="GET" id="filtro">
    <input type="hidden" name="page" value="lista_condominio">
    <input type="text" class="form-control col-sm-5 mr-sm-2 termo1 mb-3" placeholder="Nome Condomínio" aria-label="Search" name="b[nome]">
    <button class="btn btn-dark col-sm-2 mr-2 ml-2 mb-3" type="submit" disabled>Buscar</button>
    <a class="btn btn-dark col-sm-2 mb-3 mr-2 ml-2" type="reset" href="<?= $url_site ?>lista_condominio">Limpar</a>
</form>
<div class="table-responsive">
    <table class="table mb-5 tabelaCondominios">
        <thead>
            <tr>
                <th scope="col">Nome</th>
                <th scope="col">Qt. Blocos</th>
                <th scope="col">Endereço</th>
                <th scope="col">Síndico</th>
                <th scope="col">Data Cad.</th>
                <th scope="col">Data Mod.</th>
                <th><a href="index.php?page=cadastro_condominio"><button class="btn btn-dark">Adicionar</button></a></th>
            </tr>
        </thead>
        <tbody>
            <?php 
            foreach($result['resultSet'] as $key=>$condominio){ 
            ?>
                <tr data-id="<?= $condominio['id']; ?>" class="condominio">
                    <td><?= $condominio['nomeCondominio'] ?></td>
                    <td><?= $condominio['qtBlocos'] ?></td>
                    <td><?= $condominio['logradouro'].', '.$condominio['numero'].', '.$condominio['bairro'].', '.$condominio['cidade'].', '.$condominio['estado'].', CEP '.$condominio['cep'] ?></td>
                    <td><?= ($condominio['nomeConselho']? $condominio['nomeConselho'] : 'Não cadastrado') ?></td>
                    <td><?= dateFormat($condominio['dataCadastro']) ?></td>
                    <td><?= dateFormat($condominio['dataModificado']) ?></td>
                    <td><a class="p-1 removerCondominio" href="#"><i class="bi bi-x-circle-fill text-danger"></i></a><a class="p-1" href="<?= $url_site ?>cadastro_condominio/<?php echo $condominio['id']; ?>"><i class="bi bi-pencil-square ml-1 text-info"></i></a><td>
                </tr>
    
            <?php } ?> 
            <tr>
                <td colspan="6">&nbsp;</td>
                <td class="totalRegistros"><?php echo 'Registros: ' . ($result['totalResults']<10? '0'. $result['totalResults'] : $result['totalResults'])?></td>
            </tr>
        </tbody>
    </table>
</div>


<div class="col-sm-12">
<?= $paginacao; ?>
</div>
