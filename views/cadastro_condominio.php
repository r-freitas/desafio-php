    <h5>Cadastro de Condomínio</h5>
    <form action="" method="post" class="col-12 mt-3 formCondominio">
        <div class="form-group">
            <label for="nomeCondominio">Nome Condomínio</label>
            <input type="text" class="form-control" id="nomeCondominio" name="nomeCondominio" value="<?= $popular['nomeCondominio'] ?>" required>
        </div>
        <div class="form-group">
            <label for="qtBlocos">Quantidade de Blocos</label>
            <input type="text" class="form-control" id="qtBlocos" name="qtBlocos" value="<?= $popular['qtBlocos'] ?>" required>
        </div>
        <section class="row">
            <div class="form-group col-lg-9">
                <label for="logradouro">Logradouro</label>
                <input type="text" class="form-control" id="logradouro" name="logradouro" value="<?= $popular['logradouro'] ?>" required>
            </div>
            <div class="form-group col-lg-3">
                <label for="numero">Número</label>
                <input type="text" class="form-control" id="numero" name="numero" value="<?= $popular['numero'] ?>">
            </div>
            <div class="form-group col-lg-6">
                <label for="bairro">Bairro</label>
                <input type="text" class="form-control" id="bairro" name="bairro" value="<?= $popular['bairro'] ?>">
            </div>
            <div class="form-group col-lg-6">
                <label for="cidade">Cidade</label>
                <input type="text" class="form-control" id="cidade" name="cidade" value="<?= $popular['cidade'] ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="estado">Estado</label>
                <select class="custom-select" name="estado" id="estado">
                    <option value="">Estado</option>
                    <?php foreach ($estados as $uf => $opcao) {
                        echo '<option value="' . $uf . '" ' . ($popular['estado'] == $uf ? 'selected' : '') . '>' . $opcao . '</option>';
                    } ?>
                </select>
            </div>
            <div class="form-group col-md-4">
                <label for="cep">CEP</label>
                <input type="text" class="form-control" id="cep" name="cep" value="<?= $popular['cep'] ?>">
            </div>
            <div class="form-group col-md-4">
                <label for="id_administradora">Administradora Responsável</label>
                <select class="custom-select" name="id_administradora" id="id_administradora">
                    <option value="">Administradora</option>
                    <?php
                    foreach ($opcaoAdm['resultSet'] as $opcao) { ?>
                        <option value="<?= $opcao['id'] ?>"><?= $opcao['nomeAdministradora'] ?></option>
                    <?php } ?>
                </select>
            </div>
        </section>
        <?php if ($_GET['id']) { ?>
            <input type="hidden" name="editar" value="<?php echo $_GET['id'] ?>">
        <?php } ?>
        <button type="submit" class="btn btn-dark buttonEnviar">Enviar</button>
    </form>